FROM ubuntu:latest

RUN apt update && apt -y upgrade && apt -y install isync ca-certificates cron && echo "* * * * * root mbsync -c /isync/mbsyncrc default 2>&1 > /dev/null" >> /etc/crontab

CMD ["cron", "-f"]